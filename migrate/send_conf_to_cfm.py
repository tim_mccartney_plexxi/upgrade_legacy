#!/usr/bin/python 

##########################################################################################
# Copyright 2017 Plexxi Inc. and its licensors.  All rights reserved.                    #
# Use and duplication of this software is subject to a separate license                  #
# agreement between the user and Plexxi or its licensor.                                 #
##########################################################################################


import os,sys,argparse,subprocess,copy
import logging,debug_util
import cacheUTIL
logging.basicConfig(filename="/var/log/migration.log",level=logging.DEBUG)
logging.basicConfig(level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(message)s")
logging.debug("DEBUG Logging activated.")

import switch_conf #class definition for switch configuration data
import restUTIL  #helper functions to send data up to CFM via REST API





#parse the command line args

# while defaults are provided in argparse, if there is no arg, no default seems to be applied so define here.


parser = argparse.ArgumentParser(description='Send Configuration to CFM, intermediate python -> 5.0')
parser.add_argument('-v','--verbose', action="store_true", dest="verbose", default=False, help='Verbose mode outputs more to screen. Default is simple view.')
parser.add_argument('-c','--custom_file', action="store", dest="custom_file", default="", help='Use alternate file for initial fabric configuration.')
parser.add_argument('-u','--username', action="store", dest="cfm_user", default="admin", help='Username for authentication at CFM.')
parser.add_argument('-p','--password', action="store", dest="cfm_pass", default="plexxi", help='Password for authentication at CFM.')
parser.add_argument('-t','--target', dest = 'theTargets', help='A CFM (Composable Fabric Manager) IP address to send these commands.')
parser.add_argument('-o','--override', dest='theOverrides', default="TBD", help='Too early in the morning for specifics.')
args = parser.parse_args()

verbose = args.verbose
theOverrides = args.theOverrides
custom_file = args.custom_file
cfm_user = args.cfm_user
cfm_pass = args.cfm_pass
theTargets = args.theTargets

# Override cache means do NOT use cached entries found in backup directories.

if (theOverrides.find("restdump") >= 0):
    print("Dumping rest messages.")
    debug_util.rest_debug()



# reads cache and tells you what switches we have, sanity check.
    
cached_switch_conf_list = cacheUTIL.file2data()

for switch_conf in cached_switch_conf_list:
    print("I live in the land of cache, %s, bling!" % switch_conf.hostname)



# Get a token, get the fabric, parse for switches, use switches uuid to get all ports
# This will be used to form a structure to pin intended configuration to.

TOKEN = restUTIL.getTOKEN(theTargets,cfm_user,cfm_pass)
if (verbose): print("TOKEN is %s" % TOKEN)

FABRIC = restUTIL.getFABRIC(theTargets,TOKEN)
if (verbose): print("FABRIC is %s" % FABRIC)

FABRICmap = FABRIC[0]
SWITCHES = FABRICmap["switches"]


def getALLLAGs():
    allLAGs_raw = restUTIL.getLAGs(theTargets,TOKEN,FABRICmap["uuid"])
    allLAGs = {}
    for LAG in allLAGs_raw:
        #allLAGs = {}
        allLAGs[LAG["name"]] = LAG["uuid"]
        #allLAGs.append(this_LAG)

    return allLAGs

def getALLLAGs_RAW():
    allLAGs_raw = restUTIL.getLAGs(theTargets,TOKEN,FABRICmap["uuid"])

    return allLAGs_raw


map_of_ports = {}

for switch in SWITCHES:
    if (verbose):
        print("RAW Switch (from FABRIC): %s : " % switch)
        print("Switch Name %s : mac: %s uuid: %s ip: %s sw_version: %s " % (switch["name"],switch["mac_address"],switch["uuid"],switch["ip_address"],switch["sw_version"]))
        
            
    
    # Not sure if I have to get "All" ports at some point TBD
    #ports = restUTIL.getFABRICPORTS_UPLINK(theTargets,TOKEN,switch["uuid"])
    #ports = restUTIL.getFABRICPORTS(theTargets,TOKEN,switch["uuid"])
    #ports = restUTIL.getACCESSPORTS_UPLINK(theTargets,TOKEN,switch["uuid"])
    ports = restUTIL.getACCESSPORTS(theTargets,TOKEN,switch["uuid"])

    newINT2UUID = {}
    
    #if (verbose):
    #    print("\tRAW Ports: %s" % ports)

    for port in ports:
        #if (verbose):
            #print("Port: %s uuid: %s Admin State: %s Link State: %s Speed %s QSFP mode %s Native VLAN %s ALL VLAN %s UNGROUPED VLAN %s" % (port["port_label"],port["uuid"],port["admin_state"],port["link_state"],port["speed"]["current"],port["qsfp_mode"],port["native_vlan"],port["vlans"],port["ungrouped_vlans"]))

        newINT2UUID[port["port_label"]] = port["uuid"]
        map_of_ports[port["uuid"]] = port
    
    # find the switch object that corresponds to this switch in the fabric, so we can populate its Interface 2 UUID map
    for switch_conf in cached_switch_conf_list:
        if (switch_conf.getMACADDR() == switch["mac_address"]):
            print("Found matching switch %s" % switch_conf.getHOSTNAME())
            switch_conf.setINT2UUIDMAP(newINT2UUID)
            

# Dump new INT display to UUID map in verbose mode to assist in debugging of something runs afoul.
            
if (verbose):
    for switch_conf in cached_switch_conf_list:
        print("INT2UUID MAP ==> %s" % switch_conf.getINT2UUIDMAP())

    
# Now go through all switches, pick up qsfp mode for each interface (the .1 of a x2 or x4, nothing in a 1x case)
# with qsfp mode of interface, grab the port data harvested for that interface, populate it with correct qsfp mode

port_data_to_set_qsfp = {}
# print("******************************")
# print("******************************")
# print(map_of_ports)
# print("******************************")
# print("******************************")

for switch_conf in cached_switch_conf_list:
    this_switch_qsfp_map = switch_conf.getQSFPMAP()
    this_switch_speed_map = switch_conf.getSPEEDMAP()
    this_switch_int2uuid_map = switch_conf.getINT2UUIDMAP()
    #print(this_switch_qsfp_map)
    #print(this_switch_int2uuid_map)
    
    for this_qsfp in this_switch_qsfp_map:
        # we assume initial state is x4, this will not be the case for sfp, but there is no need for setting qsfp mode in that situation.
        target_inter = ((this_qsfp - 1) / 4) + 1
        target_inter = str(target_inter) + ".1"        
        try:
            uuid_to_set = this_switch_int2uuid_map[target_inter]
            # seed data
            #print("uuid_to_set %s" % uuid_to_set)
            port_data_to_set = copy.deepcopy(map_of_ports[uuid_to_set])
            #print("port_data_to_set %s" % port_data_to_set)
            # set real values
            old_int_name = str("xp") + str(this_qsfp)

            
            if (this_switch_qsfp_map[this_qsfp] == 4):
                if (this_switch_speed_map[target_inter] == "25g"):
                    port_data_to_set["qsfp_mode"] = "qsfp_4x25_gbps"                    
                if (this_switch_speed_map[target_inter] == "10g"):
                    port_data_to_set["qsfp_mode"] = "qsfp_4x10_gbps"
            if (this_switch_qsfp_map[this_qsfp] == 2):            
                if (this_switch_speed_map[target_inter] == "50g"):
                    port_data_to_set["qsfp_mode"] = "qsfp_2x50_gbps"
            try:
                if (this_switch_qsfp_map[this_qsfp] == 1):
                    if (this_switch_speed_map[target_inter] == "100g"):
                        port_data_to_set["qsfp_mode"] = "qsfp_1x100_gbps"
                    if (this_switch_speed_map[target_inter] == "40g"):
                        port_data_to_set["qsfp_mode"] = "qsfp_1x40_gbps"
            except KeyError:
                # Due to interface numbers changing after qsfp change we have to eliminate ".1" here.
                if (this_switch_qsfp_map[this_qsfp] == 1):
                    target_inter = target_inter.replace(".1","")
                    #print("Target_inter altered %s %s" % (target_inter,new_target_inter))
                    #target_inter = new_target_inter
                    if (this_switch_speed_map[target_inter] == "100g"):
                        port_data_to_set["qsfp_mode"] = "qsfp_1x100_gbps"
                    if (this_switch_speed_map[target_inter] == "40g"):
                        port_data_to_set["qsfp_mode"] = "qsfp_1x40_gbps"

            #sys.stdout.write("target_inter %s " % (target_inter))
            #sys.stdout.write("QSFP is %s " % (this_switch_qsfp_map[this_qsfp]))
            #sys.stdout.write("speed %s\n" % (this_switch_speed_map[target_inter]))


                
            # accumulate for bulk setting
            port_data_to_set_qsfp[uuid_to_set] = port_data_to_set
            if (verbose): print("%s ==> %s\n\n" % (uuid_to_set, port_data_to_set_qsfp[uuid_to_set]))
        except KeyError:
            # we only have this here for debugging. Usually we do NOT care if there is a keyerror as this means that QSFP doesn't need reconfiguration
            # either because its an sfp *mostly* or because it hasn't changed *maybe*.
            #print("KeyError %s %s" % (this_qsfp,target_inter))
            continue

print("Harvested default port data %s units and new port data after qsfp modes is %s units." % (len(map_of_ports),len(port_data_to_set_qsfp)) )

# diff print of qsfp modes
prune_list = []
for interface in port_data_to_set_qsfp:
    if (map_of_ports[interface] == port_data_to_set_qsfp[interface]):
        if (verbose): print("%s is the same same, no-op, add to remove list" % interface)
        prune_list.append(interface)
    else:
        if (verbose): print("was %s wants %s" % (map_of_ports[interface]["qsfp_mode"],port_data_to_set_qsfp[interface]["qsfp_mode"]))
        
for interface in prune_list:
    if (verbose): sys.stdout.write("removed %s " % interface)
    del port_data_to_set_qsfp[interface]

# set qsfp mode

for port in port_data_to_set_qsfp:
    #Because qsfp's might ultlimately have no dot extension, when setting qsfp you'll need, so post-pend it temporarly for this purpose.
    # putting ".1" back on target port as its name will be this before it really becomes 1x.
    if ((port_data_to_set_qsfp[port]["qsfp_mode"] == "qsfp_1x100_gbps") or (port_data_to_set_qsfp[port]["qsfp_mode"] == "qsfp_1x40_gbps")):
        port_data_to_set_qsfp[port]["port_label"] = port_data_to_set_qsfp[port]["port_label"] + ".1"

    if (verbose): sys.stdout.write("setting %s with %s ..." % (port,port_data_to_set_qsfp[port]))
    restUTIL.setPORTqsfp(theTargets,TOKEN,port_data_to_set_qsfp[port])
    if (verbose): sys.stdout.write("done.\n")
    

# reharvest interface uuids because there is either a completely new set, or a reduced list due to application of actual qsfp modes.

map_of_ports = {}

for switch in SWITCHES:
    if (verbose):
        print("RAW Switch (from FABRIC): %s : " % switch)
        print("Switch Name %s : mac: %s uuid: %s ip: %s sw_version: %s " % (switch["name"],switch["mac_address"],switch["uuid"],switch["ip_address"],switch["sw_version"]))
        
            
    
    # Not sure if I have to get "All" ports at some point TBD
    #ports = restUTIL.getFABRICPORTS_UPLINK(theTargets,TOKEN,switch["uuid"])

    
    portsFabric = restUTIL.getFABRICPORTS(theTargets,TOKEN,switch["uuid"])
    portsUplink = restUTIL.getACCESSPORTS_UPLINK(theTargets,TOKEN,switch["uuid"])
    portsSec = restUTIL.getACCESSPORTSsec(theTargets,TOKEN,switch["uuid"])
    ports = restUTIL.getACCESSPORTS(theTargets,TOKEN,switch["uuid"])
    
    # print("port for switch %s" % switch["mac_address"])
    # for port in ports:
    #     print("port %s uuid %s" % (port["port_label"],port["uuid"]))
    # print("portSec for switch %s" % switch["mac_address"])
    # for port in portsSec:
    #     print("port %s uuid %s" % (port["port_label"],port["uuid"]))
    # print("portFabric for switch %s" % switch["mac_address"])
    # for port in portsFabric:
    #     print("port %s uuid %s" % (port["port_label"],port["uuid"]))
    # print("portUplink for switch %s" % switch["mac_address"])
    # for port in portsUplink:
    #     print("port %s uuid %s" % (port["port_label"],port["uuid"]))
        
    newINT2UUID = {}
    
    for port in (ports + portsFabric + portsUplink + portsSec):
        #if (verbose):
            #print("Port: %s uuid: %s Admin State: %s Link State: %s Speed %s QSFP mode %s Native VLAN %s ALL VLAN %s UNGROUPED VLAN %s" % (port["port_label"],port["uuid"],port["admin_state"],port["link_state"],port["speed"]["current"],port["qsfp_mode"],port["native_vlan"],port["vlans"],port["ungrouped_vlans"]))

        newINT2UUID[port["port_label"]] = port["uuid"]
        map_of_ports[port["uuid"]] = port

        #print("port %s uuid %s" % (port["port_label"],port["uuid"]))
    
    # find the switch object that corresponds to this switch in the fabric, so we can populate its Interface 2 UUID map
    for switch_conf in cached_switch_conf_list:
        if (switch_conf.getMACADDR() == switch["mac_address"]):
            print("Found matching switch %s" % switch_conf.getHOSTNAME())
            #print("int2uuid before %s\nint2uuid after %s" % (switch_conf.getINT2UUIDMAP(),newINT2UUID))
            switch_conf.setINT2UUIDMAP(newINT2UUID)


# Dump new INT display to UUID map in verbose mode to assist in debugging of something runs afoul.
            
#if (verbose):
#    for switch_conf in cached_switch_conf_list:
#        print("INT2UUID MAP ==> %s" % switch_conf.getINT2UUIDMAP())

        
        
# Now we can applied actual speeds and enabled states to the interfaces (which now should be the appropriate number as qsfp modes above are applied
port_data_to_set = {}

for switch_conf in cached_switch_conf_list:
    this_switch_vlan_map = switch_conf.getVLANMAP()
    this_switch_speed_map = switch_conf.getSPEEDMAP()
    this_switch_adminstate_map = switch_conf.getADMINSTATEMAP()
    this_switch_int2uuid_map = switch_conf.getINT2UUIDMAP()

    #print("int2uuid MAP for %s %s  is %s" % (switch_conf.getHOSTNAME(),switch_conf.getMACADDR(),this_switch_int2uuid_map))
    for interface in this_switch_speed_map:
        sys.stdout.write("\nSwitch %s %s " % (switch_conf.getHOSTNAME(),switch_conf.getMACADDR()))
        sys.stdout.write("Interface %s " % (interface))
        sys.stdout.write("Speed %s " % (this_switch_speed_map[interface]))
        sys.stdout.write("State %s " % (this_switch_adminstate_map[interface]))
        try:
            sys.stdout.write("UUID %s" % (this_switch_int2uuid_map[interface]))
        except KeyError:
            print("this_switch_int2uuid_map %s" % this_switch_int2uuid_map)
        # get this port's current data harvested from CFM
        this_port = map_of_ports[this_switch_int2uuid_map[interface]]

        # speed and VLAN for Access ports are set here
        # speed and VLAN for FABRIC PORTS are not configurable so we won't try
        if (this_port["type"] != "fabric" ):
            this_speed = this_switch_speed_map[interface]
            this_speed = this_speed.replace("g","000")
            this_port["speed"]["current"] = int(this_speed)
            # Vlan parsing from cached data file and provided to json for setting
            try:
                if (verbose): print(" NATIVE VLAN INFO: %s" % this_switch_vlan_map[interface]["native"])
                this_port["native_vlan"] = int(this_switch_vlan_map[interface]["native"])
                if (verbose): print("VLAN INFO: %s" % this_switch_vlan_map[interface]["vlans"])
                ungrouped_vlans_str = str(this_switch_vlan_map[interface]["vlans"])
                ungrouped_vlans_str = ungrouped_vlans_str.replace("[","")
                ungrouped_vlans_str = ungrouped_vlans_str.replace("]","")

                #print("----%s----" % ungrouped_vlans_str)
                this_port["ungrouped_vlans"] = ungrouped_vlans_str
            except (NameError,KeyError):
                try:
                    if (verbose):
                        print("no parsable non-native vlans info %s" % this_switch_vlan_map[interface])
                except KeyError:
                    if (verbose):
                        print("no interface %s could be found" % interface)
        # state gets set here.
        this_port["admin_state"] = this_switch_adminstate_map[interface]

        
        # Actual set of speed (conditionally from above), vlans and native vlans and enabled state
        #if (this_port["ungrouped_vlans"] != ""):
        # REMOVE COMMENT TO MAKE ACTION HAPPEN!!!!!! TBD!!!!
        restUTIL.setPORT(theTargets,TOKEN,this_port)
        
# Now we will recreate LAGs
for switch_conf in cached_switch_conf_list:
    this_switch_vlan_map = switch_conf.getVLANMAP()
    this_switch_speed_map = switch_conf.getSPEEDMAP()
    this_switch_adminstate_map = switch_conf.getADMINSTATEMAP()
    this_switch_int2uuid_map = switch_conf.getINT2UUIDMAP()

    this_switch_mlag_vlanmap = switch_conf.getMLAGVLANMAP()
    this_switch_mlag_list = switch_conf.getMLAGLIST()

    print("")
    for this_mlag in this_switch_mlag_list:
        print("Switch %s %s MLAG Interface %s contains %s" % (switch_conf.getHOSTNAME(),switch_conf.getMACADDR(),this_mlag["shared_lag_name"],this_mlag["port_list"]))

        native_vlan = 1
        try:
            native_vlan = this_switch_mlag_vlanmap[this_mlag["shared_lag_name"]]["native"]
        except KeyError:
            native_vlan = 1

        ungrouped_vlans_str = ""
        try:
            ungrouped_vlans_str = str(this_switch_mlag_vlanmap[this_mlag["shared_lag_name"]]["vlans"])
            ungrouped_vlans_str = ungrouped_vlans_str.replace("[","")
            ungrouped_vlans_str = ungrouped_vlans_str.replace("]","")        
        except KeyError:
            ungrouped_vlans_str = ""
            
        mlag_name = this_mlag["shared_lag_name"]

        port_uuids = []
        port_list = this_mlag["port_list"]
        for port in port_list:
            port_uuid = ""
            try:
                port_uuid = this_switch_int2uuid_map[port]
            except KeyError:
                port_uuid = ""
            port_uuids.append(port_uuid)
                
        new_lag = {
            "native_vlan": native_vlan,
            "description": "",
            "mac_learning_configuration": {
                "learning_limit": 1,
                "aging": False,
                "violation_action": "restrict",
                "mode": "dynamic"
            },
            "mac_learning_attachments": [],
            "vlan_group_uuids": [],
            "lacp_fallback": {
            },
            "mac_learning_use_default_configuration": True,
            "port_properties": [
                {
                    "lacp": {
                        "priority": 100,
                        "intervals": {
                            "slow": 30,
                            "fast": 1
                    },
                    "aggregate_port_limits": {
                        "minimum": 2,
                        "maximum": 8
                    },
                    "mode": "active"
                    },
                    "speed": {
                        "current": (int(this_switch_speed_map[port].strip("g"))*1000)
                    },
                    "port_uuids": port_uuids
                }
            ],
            "ungrouped_vlans": ungrouped_vlans_str,
            "name": mlag_name
        }
    
        # LAG/MLAGS!!
        # before creating lags, we need to determine what lags are already present (we'll need to do this after every lag creation too)
        # because if a lag exists we have to "PUT" to it, rather than "POST".

        lagmap = getALLLAGs()
        laglist_raw = getALLLAGs_RAW()
        all_port_uuids = port_uuids
        existing_uuid = ""
        existing_lag = {}
        port_prop_list = []
        lacp_fallback_existing = {}
        
        if (new_lag["name"] in lagmap.keys()):

            if (verbose):
                print("lag already present, use put rather than post")
                
            for name in lagmap.keys():
                if (name == new_lag["name"]):
                    existing_uuid = lagmap[name]
                    for raw_lag in laglist_raw:
                        #print("raw_lag %s" % raw_lag)
                        if (raw_lag["uuid"] == existing_uuid):
                            port_prop_list = raw_lag["port_properties"]
                            lacp_fallback_existing = raw_lag["lacp_fallback"]
                            for port_prop in port_prop_list:
                                all_port_uuids = list(set(port_prop["port_uuids"] + all_port_uuids))
                                #print("for lag %s all_port_uuids %s" % (name,all_port_uuids))

           # print("newlag %s" % new_lag)
           # exit(0)
            new_lag["port_properties"][0]["port_uuids"] = all_port_uuids # There should be only one element in list
            new_lag["lacp_fallback"] = lacp_fallback_existing
            new_lag["mlag"] = True
            #print("new_lag %s" % new_lag)
            restUTIL.putLAG(theTargets,TOKEN,new_lag,existing_uuid)
        else:
            if (verbose):
                print("NEWLAG : %s \n\n" % new_lag)
            restUTIL.postLAG(theTargets,TOKEN,new_lag)


# laglist_raw = getALLLAGs_RAW()
# for lag in laglist_raw:
#     print("start============================")
#     print("final lag %s" % lag)
#     print("==================")    
#     for each_port_prop in lag["port_properties"]:
#         print("each_port_prop %s" % each_port_prop)
#     print("==================")
#     print("stop============================")
        
exit(0)



    
    

